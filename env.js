const ENV = {
  ipushpull: {
    api_url: "https://beta.ipushpull.com/api",
    auth_url: "https://uat-coex-auth.ipushpull.com",
    app_url: "https://uat-coex.ipushpull.com",
    api_key: "Db9ovF47dpr5s68RZA9Rck5nCbGrRQJMRsmxIfRU",
    api_secret: "7qJLWcIu1qC5v19VHtXF1YqPrs3MzqLyujIiZEkXu77n4JiLsHNXOOPz71jrTNwvWBzW2dCjZ5Xh3meBjpd4doNpk8Pg055MbAkWcvSwm9qbXrs3pOSpYNH8cZV8W0cW",
    hsts: false,
    storage_prefix: "ipp-uat-coex",
    storage_host: "ipushpull.com",
    transport: "polling",
    client_version: "",
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: "auth",
          },
          images: [],
          theme: {
            ui: {
              show_forgotten_password: false
            },
          },
          logo: "enterprise/img/RVTraderLogin.png",
        },
        embed: {
          ipushpull: {
            client_version: "embed",
          },
        },
        mobile: {
          ipushpull: {
            storage_prefix: "ipp_enterprise_mobile",
            client_version: "mobile",
            api_key: "lZF7uY9qv16S3ZnSvWNEUKBprsF7J6qskuTf4d90",
            api_secret: "FlWIRFrYZAudn6jE8CAkEMucPR8qms3qMDHliopMdtaJDKaAjILaGTmqDc3Z7VcSbUo95B1C60znIoECTbzFkGgRFAbcVzZR8wJgeIzXbcYN68XBRzR7yyqLH077xHGW",
          },
          client_application: "mobile",
          theme: {
            logo: {
              light: "enterprise/img/RVTrader32x32.png",
              dark: "enterprise/img/RVTrader32x32_dark.png",
            },  
          },
        },
        client: {
          ipushpull: {
            client_version: "client",
          },
          stripe: {
            key: "",
          },
          disclaimer: [
            "COEX Partners is a division of Tullett Prebon Financial Services LLC (“Tullett Prebon”), an introducing broker registered with the U.S. Commodity Futures Trading Commission and a broker-dealer registered with the U.S. Securities and Exchange Commission.  This communication has been prepared for information purposes only.  It does not constitute a recommendation or solicitation to buy, sell, subscribe for or underwrite any financial instrument or product. Its information is confidential and may not be disclosed or reproduced in whole or part under any circumstances without the prior written consent of Tullett Prebon. The information or opinions contained in this communication are obtained or derived from sources generally believed to be reliable and in good faith, but Tullett Prebon makes no representations as to their accuracy or completeness. Opinions and views expressed are subject to change without notice, as are prices and availability, which are indicative only.  There is no obligation to notify recipients of any changes to this information or to do so in the future. No responsibility or liability is accepted for the use of or reliance on the information, strategies, tools, or services presented in this document.",
            "This communication is not directed to, or intended for distribution to or use by, any person or entity who is a citizen or resident of or located in any locality, state, country or other jurisdiction where such distribution, publication, availability or use would be contrary to law or regulation or which would subject Tullett Prebon or its affiliates to any registration or licensing requirement within such jurisdiction.",
            "Tullett Prebon Financial Services LLC is a member of FINRA, NFA and the Securities Investor Protection Corp.",
            "NEITHER TULLETT PREBON NOR ANY OF TULLETT PREBON’S EMPLOYEES, OFFICERS, DIRECTORS, CONTRACTORS, REPRESENTATIVES, ATTORNEYS, ACCOUNTS, SOFTWARE PROVIDERS, AGENTS OR SUBCONTRACTORS SHALL BE LIABLE TO YOU FOR ANY LOSS OF PROFIT, DATA, BUSINESS OR GOODWILL OR FOR ANY INDIRECT OR CONSEQUENTIAL LOSS OR DAMAGE ARISING IN CONNECTION WITH THE SYSTEM OR THIS AGREEMENT (IN EACH CASE WHETHER ARISING FROM NEGLIGENCE, BREACH OF CONTRACT, INDEMNITY OR OTHERWISE) EVEN IF TULLETT PREBON HAS BEEN NOTIFIED OF THE POSSIBILITY OF SUCH DAMAGE OR LOSS.",
            "NEITHER TULLETT PREBON NOR ANY OF TULLETT PREBON’S EMPLOYEES, OFFICERS, DIRECTORS, CONTRACTORS, REPRESENTATIVES, ATTORNEYS, ACCOUNTS, SOFTWARE PROVIDERS, AGENTS OR SUBCONTRACTORS SHALL BE LIABLE FOR (I) THE CAPACITY, RELIABILITY, AVAILABILITY, ACCURACY OR PERFORMANCE OF THE SYSTEM OR THE ACTS OR OMISSIONS OF OTHER USERS THEREOF; (II) THE COMMERCIAL ADVISABILITY OF ANY ORDER, REVOCATION (OF AN ORDER) OR TRANSACTION; (III) THE RELIABILITY OR ACCURACY OF ANY INFORMATION SUPPLIED BY ANY PARTY TO THIS AGREEMENT IN RELATION TO ANY ORDER, REVOCATION OR TRANSACTION; (IV) ANY OTHER OBLIGATION OR LIABILITY ARISING IN RELATION TO AN ORDER, REVOCATION OR TRANSACTION; OR (V) FOR THE CAPACITY, RELIABILITY OR PERFORMANCE OF YOU OR ANY OTHER USER THEREOF WITH REGARD TO ANY ORDER, REVOCATION OR TRANSACTION.",
            "THE AGGREGATE LIABILITY OF TULLETT PREBON AND TULLETT PREBON’S EMPLOYEES, OFFICERS, DIRECTORS, CONTRACTORS, REPRESENTATIVES, ATTORNEYS, ACCOUNTS, SOFTWARE PROVIDERS, AGENTS AND SUBCONTRACTORS TO YOU UNDER THIS AGREEMENT, WHETHER ARISING FROM NEGLIGENCE, BREACH OF CONTRACT OR OTHERWISE, SHALL NOT IN ANY EVENT EXCEED US$10,000.",
            "You shall defend, indemnify and hold harmless Tullett Prebon and its employees, officers, directors, contractors, representatives, attorneys, accounts, software providers, agents or subcontractors (each an “Tullett Prebon Indemnified Party”) from and against any losses to which any Tullett Prebon Indemnified Party may become subject, insofar as such losses arise out of or in connection with, or are based upon any proceeding against an Tullett Prebon Indemnified Party that arises out of or relates to (i) any access, use, or misuse of the system or the information by you or by any person accessing the system using your access details, or (ii) your failure to settle or otherwise perform or comply with the terms of any transaction; provided, that, in each case, such losses do not result from Tullett Prebon s fraud, gross negligence or wilful misconduct."
            ],
          title_suffix: 'COEX',
          theme: {
            upgrade: {
              version: "2021.7.6",
              type: "minor",
              release: {
                label: "Find out more",
                help_doc: "https://support.ipushpull.com",
              },
              message:
                "",
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg",
              },
            },
            fonts: [
              {
                name: "Ubuntu Mono",
                url: "https://fonts.googleapis.com/css2?family=Ubuntu+Mono:wght@400;700&display=swap"
              },
            ],
            mobile_ui: {
              nav_create: false,
              nav_help: false,
              nav_share: false,
              nav_setup: false,             
              nav_home_startup: true, 

              profile_tab_integrations: false,
              profile_tab_keys: false,

              folders_and_pages_types: [8],
              folders_and_pages_can_pin_folder: true,
              folders_and_pages_tab_recent: false,
              folders_and_pages_tab_favs: false,              

              toolbar_fav: false,
              toolbar_columns: false,
              toolbar_highlights: false,
              toolbar_tracking: false,
              toolbar_filters: false,
              toolbar_sort: false,
              toolbar_share: false,              
              toolbar_menu_view_fit_height: false,
              toolbar_menu_view_fit_contain: false,    
              toolbar_menu_view_show_headings: false,
              toolbar_menu_view_show_row_selection: false,
              toolbar_menu_view_show_gridines: false,
              toolbar_menu_view_show_cell_bar: false,
              toolbar_menu_data: false,
              toolbar_menu_page: false,     
              toolbar_open_page: false,
              
              grid_menu_copy: false,
              grid_menu_cell: false,                        

              user_theme_selection: false,
              workspaces_show_views: false,
              workspaces_page_update_format: "HH:mm:ss",
              workspaces_open_page: false,
            },
            ui: {
              nav_workspaces: true,
              nav_folders: true,
              nav_create: false,
              nav_help: false,
              nav_share: false,
              nav_setup: false,
              nav_setup_lsd: true,
              nav_setup_dod: true,
              nav_setup_ddn: true,
              nav_setup_caw: true,
              nav_setup_admin: true,
              nav_home_startup: true, 
              nav_profile_download: false,

              profile_tab_integrations: false,
              profile_tab_keys: false,

              folders_and_pages_types: [8],
              folders_and_pages_can_pin_folder: true,
              folders_and_pages_can_leave_folder: true,
              folders_and_pages_tab_recent: false,
              folders_and_pages_tab_favs: false,

              toolbar: true,
              toolbar_fav: false,
              toolbar_autosave: true,
              toolbar_columns: false,
              toolbar_highlights: false,
              toolbar_tracking: false,
              toolbar_filters: false,
              toolbar_sort: false,
              toolbar_views: true,
              toolbar_share: false,

              toolbar_menu_view: true,
              toolbar_menu_view_fit: true,
              toolbar_menu_view_fit_scroll: true,
              toolbar_menu_view_fit_width: true,
              toolbar_menu_view_fit_height: false,
              toolbar_menu_view_fit_contain: false,

              toolbar_menu_view_show: true,
              toolbar_menu_view_show_headings: false,
              toolbar_menu_view_show_row_selection: false,
              toolbar_menu_view_show_row_highlight: true,
              toolbar_menu_view_show_gridines: false,
              toolbar_menu_view_show_cell_bar: false,
              toolbar_menu_view_show_filters: true,
              toolbar_menu_view_freeze: false,

              toolbar_menu_data: false,

              toolbar_menu_page: false,

              page_views: true,

              grid_menu_copy: false,
              grid_menu_filter: true,
              grid_menu_sort: true,
              grid_menu_freeze: true,
              grid_menu_rowscols: true,
              grid_menu_cell: false,
              grid_menu_share: false,
              
              workspaces_icon_full_screen: false,
              workspaces_toolbar: false,              
              workspaces_show_views: true,
              workspaces_page_update_format: "HH:mm:ss",
              workspaces_open_page: false,
              workspaces_show_tabbed_tile_history: false,

              user_theme_selection: false,
            },
            logo: {
              light: "enterprise/img/RVTrader32x32.png",
              dark: "enterprise/img/RVTrader32x32_dark.png",
            },
          },
          data_sources: [
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
                  iframe: "https://test.ipushpull.com/help.php?id=647790593",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: -1,
              name: "Database",
              key: "db",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/db.svg",
              // help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
                  iframe: "https://test.ipushpull.com/help.php?id=148865025",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: -1,
              name: "API",
              key: "api",
              logo: "",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
          ],
          client_apps: [
            {
              id: 2,
              name: "Desktop App",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                  iframe: "https://test.ipushpull.com/help.php?id=681705505",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
              appType: ["lsd"],
            },
            {
              id: 41,
              name: "Mobile App",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/qxv9HZC1",
                  iframe: "https://test.ipushpull.com/help.php?id=650248196",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
                  iframe: "https://test.ipushpull.com/help.php?id=156074055",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: 38,
              name: "Microsoft Teams App",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                },
              },
            },
            {
              id: 42,
              name: "Microsoft Teams Bot",
              key: "teamsBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                },
              },
            },
            {
              id: 7,
              name: "Slack Bot",
              key: "slack",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/X5EwR7Dy",
                  iframe: "https://test.ipushpull.com/help.php?id=156598336",
                },
              },
            },
            {
              id: 14,
              name: "Symphony App",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                },
              },
            },
            {
              id: 43,
              name: "Symphony Bot",
              key: "symphonyBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                },
              },
            },

            // {
            //   id: -1,
            //   name: "WordPress",
            //   key: "wp",
            //   logo: {
            //     light:
            //       "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/wp.svg",
            //     dark:
            //       "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/wp-white.svg",
            //   },
            //   help_doc: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
            // },
          ],
          lsd_apps: [
            {
              id: 2,
              name: "Desktop app",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: [
                {
                  title: "Client App overview",
                  url: "https://ipushpull.atlassian.net/l/c/PHy2bZn0",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/7j4QXJHj",
                },
              ],
            },
            {
              id: 41,
              name: "Mobile app",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              help: [
                {
                  title: "Install the Excel Add-in",
                  url: "https://ipushpull.atlassian.net/l/c/NXJ1Js3d",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/l/c/WHeWjzmk",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/L9oPttf9",
                },
              ],
            },
            {
              id: 38,
              name: "Microsoft Teams",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              help: [
                {
                  title: "Install the Teams app",
                  url: "https://ipushpull.atlassian.net/l/c/TuecR7Ar",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/l/c/U0CeB7fZ",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/sZorb7Kt",
                },
              ],
            },
            {
              id: 14,
              name: "Symphony",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              help: [
                {
                  title: "Symphony app overview",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/VtU22Dzj",
                },
              ],
            },
          ],
        },
      },
    },
  },
  sso_only: false,
  sso_url: "",
  on_prem: true,
  billing: false,
  help_docs: {
    livestreaming_data_source_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
      iframe: "https://test.ipushpull.com/help.php?id=647790593",
    },
    livestreaming_data_source_db: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
      iframe: "https://test.ipushpull.com/help.php?id=148865025",
    },
    livestreaming_data_source_api: {
      desk: "https://ipushpull.atlassian.net/wiki/x/CYCgK",
      iframe: "https://test.ipushpull.com/help.php?id=681607177",
    },
    livestreaming_client_app_ipp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
      iframe: "https://test.ipushpull.com/help.php?id=681705505",
    },
    livestreaming_client_app_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
      iframe: "https://test.ipushpull.com/help.php?id=156074055",
    },
    livestreaming_client_app_symphony: {
      desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
      iframe: "https://test.ipushpull.com/help.php?id=695959587",
    },
    livestreaming_client_app_wp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
      iframe: "https://test.ipushpull.com/help.php?id=696352875",
    },
    livestreaming_client_app_teams: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
      iframe: "https://test.ipushpull.com/help.php?id=678494209",
    },
    notifications_builder_excel_data_source: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDmI",
      iframe: "https://test.ipushpull.com/help.php?id=551976961",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/H4BeHg",
      iframe: "https://test.ipushpull.com/help.php?id=509509663",
    },
    notifications_builder_slack_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/JgBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542438",
    },
    notifications_builder_symphony_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/LwBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542447",
    },
    symphony_overview: {
      desk: "https://ipushpull.atlassian.net/wiki/x/UYCHDQ",
      iframe: "https://test.ipushpull.com/help.php?id=226984017",
    },
    user_authoriziations: {
      desk: "https://ipushpull.atlassian.net/l/c/GApuWLAA",
      iframe: "https://test.ipushpull.com/help.php?id=631701505",
    },
  },
};
